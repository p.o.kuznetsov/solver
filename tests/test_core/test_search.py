import unittest

from solver.core import search_max, search_min


class TestSearch(unittest.TestCase):
    def test_search_max(self):
        test_data = [
            [(1, 2, 3, 4, 5, 4, 5), (0, 4)],
            [(0, 1, 2, 3, 4, 5, 4, 5), (1, 5)],
            [(1, 2, 3, 1, 100, 101, 2, 3), (3, 5)],
            [(-100, 2, 3, 4, 5, 4, 5), (1, 4)],
            [(-100, -50, -30, -100), (1, 2)],
            [(1, 2, 3, -100, 100), (0, 2)],
            [(1, 2, 3, -100, 150, 149), (3, 4)],
        ]
        for input_value, expected in test_data:
            data = search_max(input_value)
            self.assertEqual(expected, data)

    def test_search_min(self):
        test_data = [
            [(5, 4, 3, 2, 1, 4), (3, 4)],
            [(-100, 5, 4, 3, 2, 1, 4), (4, 5)],
            [(5, 4, 3, 5, 1, 0, -1, -100), (5, 7)],
            [(100, -2, -3, 4, 5, 4, 5), (4, 5)],
            [(-10, -30, -50, -40), (0, 2)],
            [(-1, -3, -5, 10, 9, -100), (4, 5)],
            [(-9, 6, 1, 10, 5, 4, 7, 5, -5, -4, -7, 1, 9), (9, 10)]
        ]
        for input_value, expected in test_data:
            data = search_min(input_value)
            self.assertEqual(expected, data)
