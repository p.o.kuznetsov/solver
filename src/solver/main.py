# app
from solver.states import StateMachine


def run():
    state_machine = StateMachine()
    while True:
        state_machine()


if __name__ == '__main__':
    run()
