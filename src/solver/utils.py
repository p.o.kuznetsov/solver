def typed_input(required_type, *args, **kwargs):
    value = input(*args)
    if value in kwargs.get('extra_values', []):
        return value
    return required_type(value)
