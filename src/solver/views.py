import sys
from random import randint

from .core.array_io import ConsoleArrayIO
from .core import search_min, search_max
from .utils import typed_input


def show_menu(menu: dict) -> int:
    sys.stdout.writelines([
        f'{k}: {v}\n' for k, v in menu.items()
    ])

    inp = typed_input(next(iter(menu.keys())).__class__, 'Enter value: ')
    if inp not in menu.keys():
        raise ValueError
    return inp


def has_accepted(message: str, y_default=True) -> bool:
    if y_default is True:
        choice = '[Y/n]'
    else:
        choice = ['y/N']

    new_line = len(message) > 100
    if new_line:
        message += '\n'
    value = input(f'{message} {choice}')

    if y_default is True:
        return value not in ('n', 'N')
    else:
        return value in ('y', 'Y')


def menu_view():
    menu = {
        1: 'Input from console',
        2: 'Random array',
        3: 'Exit',
    }
    return show_menu(menu)


def input_view(value):
    if not has_accepted(f'Is value correct? {value}.'):
        raise ValueError()


def console_input_view():
    value = ConsoleArrayIO().load()
    input_view(value)
    return value


def calculate_view(value):
    menu = {1: 'MIN', 2: 'MAX'}
    selected = show_menu(menu)

    search_function = {
        1: search_min,
        2: search_max,
    }.get(selected)

    left, right = search_function(value)
    print(f'left_index: {left}, right_index: {right}')
    print(f'subset: {value[left:right + 1]}')


def initial_view():
    print(f'Start programm.')


def on_error():
    print(f'Incorrect action.')


def input_generate_view():
    generated = [randint(-10, 10) for i in range(randint(10, 500))]
    print(f'Length={len(generated)}')
    input_view(generated)
    return generated
