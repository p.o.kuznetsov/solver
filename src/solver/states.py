# built-in
import sys

# app

from solver import views

STORAGE = {}
ARRAY = 'ARRAY'


class StateMachine:
    INITIAL = 'INITIAL'
    MENU = 'MENU'
    INPUT_CONSOLE = 'INPUT_CONSOLE'
    INPUT_GENERATE = 'INPUT_GENERATE'
    CALCULATE = 'CALCULATE'
    ERROR = 'ERROR'
    EXIT = 'EXIT'

    def __init__(self):
        self._state = self.INITIAL
        self._state_history = []

    def __call__(self, *args, **kwargs):
        self._state_history.append(self._state)
        try:
            {
                self.INITIAL: self.on_initial,
                self.MENU: self.on_menu,
                self.INPUT_CONSOLE: self.on_input_console,
                self.CALCULATE: self.on_calculate,
                self.INPUT_GENERATE: self.on_input_generate,
                self.ERROR: self.on_error,
                self.EXIT: self.on_exit,
            }.get(self._state)()

        except ValueError:
            self._state = self.ERROR

    def on_menu(self):
        view_value = views.menu_view()
        state = {
            1: self.INPUT_CONSOLE,
            2: self.INPUT_GENERATE,
            3: self.EXIT,
        }.get(view_value)

        self._state = state

    def on_initial(self):
        views.initial_view()
        self._state = self.MENU

    def on_input_console(self):
        value = views.console_input_view()
        self._state = self.CALCULATE
        STORAGE[ARRAY] = value

    def on_input_generate(self):
        value = views.input_generate_view()
        self._state = self.CALCULATE
        STORAGE[ARRAY] = value

    def on_calculate(self):
        views.calculate_view(STORAGE[ARRAY])
        self._state = self.MENU

    def on_error(self):
        views.on_error()
        self._state_history.pop()
        self._state = self._state_history.pop()

    def on_exit(self):
        sys.exit(0)
