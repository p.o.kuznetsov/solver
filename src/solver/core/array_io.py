import sys
from abc import ABC, abstractmethod

from ..utils import typed_input


class ArrayIO(ABC):
    def __init__(self):
        self._array = None

    @abstractmethod
    def load(self, **kwargs):
        pass


class ConsoleArrayIO(ArrayIO):
    def load(self, **kwargs):
        values = []
        sys.stdout.write('Enter array values. Use empty value to stop the input.\n')
        while True:
            value = typed_input(float, extra_values=[''])
            if value == '':
                return values
            values.append(value)
