from typing import Tuple


def search_min(value: list) -> Tuple[int, int]:
    total = abs(value[0] + value[1])
    current_summ = value[0]
    left_index = 0
    right_index = 1
    left_current_index = 0
    for i in range(1, len(value)):
        current_value = value[i]
        previous_value = value[i - 1]

        current_summ += current_value

        if current_value >= previous_value:
            left_current_index = i
            current_summ = current_value
            continue

        if current_summ > total:
            left_current_index = i - 1
            left_index = left_current_index
            total = previous_value + current_value
            current_summ = total
            right_index = i

        if current_summ < total:
            if current_value < 0 <= previous_value:
                left_current_index = i - 1
            left_index = left_current_index
            total = current_summ
            right_index = i

    return left_index, right_index


def search_max(value: list) -> Tuple[int, int]:
    total = -abs(value[0] + value[1])
    current_summ = value[0]
    left_index = 0
    right_index = 1
    left_current_index = 0
    for i in range(1, len(value)):
        current_value = value[i]
        previous_value = value[i - 1]

        current_summ += current_value

        if current_value <= previous_value:
            left_current_index = i
            current_summ = current_value
            continue

        if current_summ > total:
            left_index = left_current_index
            total = current_summ
            right_index = i

        if previous_value <= 0:
            left_current_index = i
            current_summ = 0

    return left_index, right_index
